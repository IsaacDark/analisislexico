package lexico;

import java_cup.runtime.Symbol;

import java.io.*;

public class AnalizadorLexico {
    public static void main(String[] args) throws IOException {
        File file = new File("src\\lexico\\CodigoObjective.txt");
        try {
            Reader lector = new BufferedReader(new FileReader(file));

            ObjectiveCLexico lexico = new ObjectiveCLexico(lector);
            String resultado = "";

            while(true){
                Symbol token = lexico.yylex();
                if(token==null || token.sym==sym.EOF){
                    System.out.println("FIN DEL ARCHIVO");
                    break;
                }
                System.out.println(token.toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
