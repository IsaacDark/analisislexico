package lexico;

public class sym {
    static final int EOF=-1;
    static final int ERROR=1;
    static final int ESPACIO=2;
    static final int MULTILINEA=3;
    static final int UNILINEA=4;
    static final int D=5;
    static final int IDENTIFICADOR=6;
    //tipo de dato
    static final int INT=10;
    static final int FLOAT=11;
    static final int DOUBLE=12;
    static final int CHAR=13;
    static final int LONG=14;
    static final int SHORT=15;
    static final int BYTE=16;
    static final int UNSIGNED=17;
    static final int BOOLEAN=18;
    static final int SIGNED=19;
    static final int VOID=20;
    //import
    static final int IMPORT=30;
    //modificador de tipo
    static final int CONST=40;
    static final int STATIC=41;
    //modificador de propiedad
    static final int PROPERTY=50;
    static final int SYNTHESIZE=51;
    static final int DYNAMIC=52;
    //modificador de acceso
    static final int PUBLIC=60;
    static final int PROTECTED=61;
    static final int PRIVATE=62;
    //estructura de control
    static final int IF=70;
    static final int ELSE=71;
    static final int DO=72;
    static final int SWITCH=73;
    static final int CASE=74;
    static final int WHILE=75;
    static final int BREAK=76;
    static final int GOTO=77;
    static final int FOR=78;
    static final int CONTINUE=79;
    static final int RETURN=80;

    //herencia
    static final int INTERFACE=90;
    static final int IMPLEMENTATION=91;
    //tipo excepcion
    static final int TRY=100;
    static final int CATCH=101;
    static final int THROW=102;
    static final int FINALLY=103;
    //delimitador de clase
    static final int CLASS=110;
    static final int END=111;
    //es protocolo
    static final int PROTOCOL=120;
    static final int REQUIRED=121;
    static final int OPTIONAL=122;

    static final int SELECTOR=130;
    static final int DELIMITADOR=131;
    static final int MAS=132;
    static final int MENOS=133;
    static final int POR=134;
    static final int ENTRE=135;
    static final int MODULO=136;
    static final int INCREMENTO=137;
    static final int DECREMENTO=138;

    //separador
    static final int COMA=139;
    static final int DOS_PUNTOS=140;

    //oparador relacional
    static final int Y_LOGICO=142;
    static final int OR_LOGICO=143;
    static final int NO=144;
    static final int MENOR_QUE=145;
    static final int MAYOR_O_IGUAL=146;
    static final int MENOR_O_IGUAL=147;
    static final int MAYOR_QUE=148;

    static final int ADN_BIT_A_BIT= 151;
    static final int BITWISE_INCLUSIVE_OR=152;
    static final int EXCLUSIVO_O=153;
    static final int COMPLEMENTO_UNARIO=154;
    static final int MAYUS_IZQUIERDA=155;
    static final int MAYUS_DERECHA=156;
    //operador de asignacion
    static final int IGUAL=157;
    static final int ADICION=158;
    static final int RESTA=159;
    static final int MULTIPLICACION=160;
    static final int DIVICION=170;

    //operador de comparacion
    static final int IGUAL_A=177;

    //operador condicional
    static final int CONDICIONAL=183;
    //operador realcional

    //operador posfix
    static final int PARENTESIS_INICIO=187;
    static final int PARENTESIS_CIERRE=188;
    static final int CORCHETE_INICIO=189;
    static final int CORCHETE_CIERRE=190;
    static final int LLAVE_INICIO=191;
    static final int LLAVE_CIERRE=192;
    static final int PUNTERO=193;
    static final int PUNTO=194;

    static final int CA=195;
    static final int AUTOPOOL=196;

}
