package lexico;
import java_cup.runtime.Symbol;
%%
%class ObjectiveCLexico
%public
%unicode
%type java_cup.runtime.Symbol
L=[a-zA-Z_]
D=[0-9]
ESPACIO=[ \t\r\n]
MULTILINEA="/*"( [^*] | (\*+[^*/]) )*\*+\/
UNILINEA="//"[^\n\r]*(\n|\r|\n\r|\r\n)
CA="@\"".*"\""
%%

{D}+ {System.out.println("Es un digito "+ yytext()); return new Symbol(sym.D);}
{ESPACIO}+ {System.out.println("Es un espacio " + yytext()); return new Symbol(sym.ESPACIO);}
{MULTILINEA} {System.out.println("Es un comentario multilinea " + yytext()); return new Symbol(sym.MULTILINEA);}
{UNILINEA} {System.out.println("Es un unilinea " + yytext()); return new Symbol(sym.UNILINEA);}
"int" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.INT);}
"float" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.FLOAT);}
"double" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.DOUBLE);}
"char" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.CHAR);}
"long" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.LONG);}
"short" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.SHORT);}
"byte" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.BYTE);}
"unsigned" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.UNSIGNED);}
"boolean" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.BOOLEAN);}
"signed" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.SIGNED);}
"void" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.VOID);}

"#import" {System.out.println("Es un import "+ yytext()); return new Symbol(sym.IMPORT);}

"const" {System.out.println("Es un modificador de tipo "+ yytext()); return new Symbol(sym.CONST);}
"static" {System.out.println("Es un modificador de tipo "+ yytext()); return new Symbol(sym.STATIC);}

"@property" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.PROPERTY);}
"@synthesize" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.SYNTHESIZE);}
"@dynamic" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.DYNAMIC);}

"@public" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PUBLIC);}
"@protected" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PROTECTED);}
"@private" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PRIVATE);}

"if" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.IF);}
"else" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.ELSE);}
"do" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.DO);}
"switch" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.SWITCH);}
"case" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.CASE);}
"while" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.WHILE);}
"break" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.BREAK);}
"goto" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.GOTO);}
"for" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.FOR);}
"continue" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.CONTINUE);}
"return" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.RETURN);}

"@interface" {System.out.println("Es herencia "+ yytext()); return new Symbol(sym.INTERFACE);}
"@implementation" {System.out.println("Es herencia "+ yytext()); return new Symbol(sym.IMPLEMENTATION);}

"@try" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.TRY);}
"@catch" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.CATCH);}
"@throw" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.THROW);}
"@finally" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.FINALLY);}

"@class" {System.out.println("Es delimitador de clase "+ yytext()); return new Symbol(sym.CLASS);}
"@end" {System.out.println("Es delimitador de clase "+ yytext()); return new Symbol(sym.END);}

"@protocol" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.PROTOCOL);}
"@required" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.REQUIRED);}
"@optional" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.OPTIONAL);}

"@selector" {System.out.println("Es selector compilado "+ yytext()); return new Symbol(sym.SELECTOR);}
"@autoreleasepool" {System.out.println("Son objetos en memoria "+ yytext()); return new Symbol(sym.AUTOPOOL);}
";" {System.out.println("Es delimitador "+ yytext()); return new Symbol(sym.DELIMITADOR);}

"+" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MAS);}
"-" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MENOS);}
"/" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.ENTRE);}
"*" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.POR);}
"%" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MODULO);}
"++" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.INCREMENTO);}
"--" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.DECREMENTO);}

"," {System.out.println("Es un separador "+ yytext()); return new Symbol(sym.COMA);}
":" {System.out.println("Es un separador "+ yytext()); return new Symbol(sym.DOS_PUNTOS);}


"&&" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.Y_LOGICO);}
"||" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.OR_LOGICO);}
"!" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.NO);}
"<" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MENOR_QUE);}
">=" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MAYOR_O_IGUAL);}
"<=" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MENOR_O_IGUAL);}
">" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MAYOR_QUE);}


"&" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.ADN_BIT_A_BIT);}
"|" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.BITWISE_INCLUSIVE_OR);}
"^" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.EXCLUSIVO_O);}
"~" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.COMPLEMENTO_UNARIO);}

"=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.IGUAL);}
"+=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.ADICION);}
"-=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.RESTA);}
"*=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MULTIPLICACION);}
"/=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.DIVICION);}
"<<=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MAYUS_IZQUIERDA);}
">>=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MAYUS_DERECHA);}

"==" {System.out.println("Es un operador de comparacion "+ yytext()); return new Symbol(sym.IGUAL_A);}

"?:" {System.out.println("Es un operador condicional "+ yytext()); return new Symbol(sym.CONDICIONAL);}

"(" {System.out.println("Es un parentesis de inicio "+ yytext()); return new Symbol(sym.PARENTESIS_INICIO);}
")" {System.out.println("Es una parentesis de cierre " + yytext()); return new Symbol(sym.PARENTESIS_CIERRE);}
"[" {System.out.println("Es un corchete de inicio " + yytext()); return new Symbol(sym.CORCHETE_INICIO);}
"]" {System.out.println("Es un corchete de cierre " + yytext()); return new Symbol(sym.CORCHETE_CIERRE);}
"{" {System.out.println("Es una llave de inicio " + yytext()); return new Symbol(sym.LLAVE_INICIO);}
"}" {System.out.println("Es una llave de cierre " + yytext()); return new Symbol(sym.LLAVE_CIERRE);}
"->" {System.out.println("Es un operador postfix "+ yytext()); return new Symbol(sym.PUNTERO);}
"." {System.out.println("Es un operador postfix "+ yytext()); return new Symbol(sym. PUNTO);}

{L}({L}|{D})* {System.out.println("Es un identificador "+ yytext()); return new Symbol(sym.IDENTIFICADOR);}
{CA} {System.out.println("Es una cadena "+ yytext()); return new Symbol(sym.CA);}
. {System.out.println("Simbolo no reconocido "); return new Symbol(sym.ERROR);}

